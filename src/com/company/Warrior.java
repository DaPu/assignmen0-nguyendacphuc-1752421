package com.company;

public class Warrior {
    private int baseHP;
    private int wp;
    private double RealHP;
    private boolean check;
    public Warrior(int baseHP, int wp) {
        if ((1 <= baseHP && baseHP <= 888) && (0 == wp || wp == 1)){
            this.baseHP = baseHP;
            this.wp = wp;
            this.check = true;
            if (wp == 1){
                this.RealHP = baseHP;
            }else {
                this.RealHP = (double) baseHP/10;
            }
        }else{
            System.out.println("Error, check your BaseHP or weapon");
            this.check = false;
        }
    }

    public int getBaseHP() {
        return baseHP;
    }

    public int getWp() {
        return wp;
    }

    public double getRealHP(){
        return RealHP;
    }

    public boolean isCheck() {
        return check;
    }
}
