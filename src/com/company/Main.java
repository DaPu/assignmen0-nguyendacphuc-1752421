package com.company;

import java.util.Scanner;

/* Name: Nguyen Dac Phuc
   Student ID: 1752421
 */
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int baseHP, wp;
        System.out.println("Enter base HP (99-999) and weapon type (0-1) of knight:");
        baseHP = in.nextInt();
        wp = in.nextInt();
        Knight knight = new Knight(baseHP, wp);
        while (!knight.isCheck()){
            baseHP = in.nextInt();
            wp = in.nextInt();
            knight = new Knight(baseHP, wp);
        }
        System.out.println("Enter base HP (1-888) and weapon type (0-1) of warrior:");
        baseHP = in.nextInt();
        wp = in.nextInt();
        Warrior warrior = new Warrior(baseHP, wp);
        while (!warrior.isCheck()){
            baseHP = in.nextInt();
            wp = in.nextInt();
            warrior = new Warrior(baseHP, wp);
        }
        getP(knight, warrior);
    }

    public static void getP(Knight a, Warrior b){
        double p = (b.getRealHP() - a.getRealHP() + 999)/2000;
        System.out.println("The odd of the knight victory is "+ p);
    }
}
